<?php

$curl = curl_init('https://crm.snapforce.com/vintage/sf_receive_request.inc.php');
$api_key=<api key>;
$api_user = <api user>;

$fetch = "format=xml&api_user=$api_user&api_key=$api_key&module=Leads&status=Active&method=fetchRecords";

curl_setopt($curl, CURLOPT_FAILONERROR, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_TIMEOUT, 5);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $fetch);
$r = curl_exec($curl);
curl_close($curl);
print_r($r);
