Simple php script to get started integrating and pull your leads from Snapforce CRM.

<h2>Snapforce CRM</h2>
<p><a href="https://www.snapforce.com/" title="Snapforce CRM">Snapforce CRM</a> is a <a href="https://www.snapforce.com/what-is-crm/" title="What is CRM">CRM Software</a> provider, referred to as a cost effective alternative to Salesforce.
